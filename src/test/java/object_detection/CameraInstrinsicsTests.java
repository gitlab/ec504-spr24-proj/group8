package object_detection;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;
import object_detection.types.*;

import java.io.FileWriter;
import java.io.IOException;
import java.io.File;

class CameraIntrinsicsTests {

    @Test
    void testCameraIntrinsicsInitialization() throws IOException {
        // Create a temporary configuration file for testing
        String tempFilePath = "test_intrinsics.csv";
        try (FileWriter writer = new FileWriter(tempFilePath)) {
            writer.write("1000.0,2000.0\n"); // FocalLength
            writer.write("500.0,500.0\n"); // PrincipalPoint
            writer.write("1280.0,720.0\n"); // ImageSize
            writer.write("1.0,0.0,0.0\n"); // K matrix rows
            writer.write("0.0,1.0,0.0\n");
            writer.write("0.0,0.0,1.0\n");
        }

        // Create a CameraIntrinsics object
        CameraIntrinsics ci = new CameraIntrinsics(tempFilePath);

        // Check its attributes
        assertArrayEquals(new float[]{1000.0f, 2000.0f}, ci.getFocalLength(), "FocalLength should be correctly initialized");
        assertArrayEquals(new float[]{500.0f, 500.0f}, ci.getPrincipalPoint(), "PrincipalPoint should be correctly initialized");
        assertArrayEquals(new float[]{1280.0f, 720.0f}, ci.getImageSize(), "ImageSize should be correctly initialized");

        double[][] expectedK = new double[][]{
            {1.0, 0.0, 0.0},
            {0.0, 1.0, 0.0},
            {0.0, 0.0, 1.0}
        };
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                assertEquals(expectedK[i][j], ci.getK().get(i, j), 0.01, "K matrix values should match");
            }
        }

        // Clean up temporary file
        new File(tempFilePath).delete();
    }
}

