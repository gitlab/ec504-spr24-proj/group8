package object_detection;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;
import object_detection.types.*;

class Point2DTests {

    @Test
    void testPoint2DCreation() {
        Point2D point = new Point2D(3.5f, 7.2f);

        assertEquals(3.5f, point.getX(), "X coordinate should match the constructor input");
        assertEquals(7.2f, point.getY(), "Y coordinate should match the constructor input");
    }

    @Test
    void testToString() {
        Point2D point = new Point2D(3.5f, 7.2f);
        String expectedString = "{3.5, 7.2}";

        assertEquals(expectedString, point.toString(), "toString should correctly represent the Point2D");
    }

    @Test
    void testMultiplePoints() {
        Point2D point1 = new Point2D(1.0f, 2.0f);
        Point2D point2 = new Point2D(3.0f, 4.0f);

        assertEquals(1.0f, point1.getX(), "Point1's X coordinate should match the constructor input");
        assertEquals(2.0f, point1.getY(), "Point1's Y coordinate should match the constructor input");

        assertEquals(3.0f, point2.getX(), "Point2's X coordinate should match the constructor input");
        assertEquals(4.0f, point2.getY(), "Point2's Y coordinate should match the constructor input");
    }
}

