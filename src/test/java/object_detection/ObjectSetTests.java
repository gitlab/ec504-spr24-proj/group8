package object_detection;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import object_detection.types.*;

import java.io.FileWriter;
import java.io.IOException;
import java.io.FileNotFoundException;
import java.util.Arrays;
import java.util.List;
import java.io.File;
import org.junit.jupiter.api.AfterEach;

class ObjectSetTests {

    private CameraIntrinsics intrinsics;
    private List<Point> pointCloud;
    private ObjectSet os;

    @BeforeEach
    void setUp() throws IOException, FileNotFoundException {
        // Create a temporary configuration file for CameraIntrinsics
        String tempIntrFile = "test_intr.csv";
        try (FileWriter writer = new FileWriter(tempIntrFile)) {
            writer.write("1000.0,2000.0\n"); // FocalLength
            writer.write("500.0,500.0\n"); // PrincipalPoint
            writer.write("1280.0,720.0\n"); // ImageSize
            writer.write("1.0,0.0,0.0\n"); // K matrix rows
            writer.write("0.0,1.0,0.0\n");
            writer.write("0.0,0.0,1.0\n");
        }

        // Initialize CameraIntrinsics from the configuration file
        intrinsics = new CameraIntrinsics(tempIntrFile);

        // Create a temporary configuration file for CameraPose
        String tempPoseFile = "test_pose.csv";
        try (FileWriter writer = new FileWriter(tempPoseFile)) {
            writer.write("1.0,2.0,3.0\n"); // Translation vector
            writer.write("1.0,0.0,0.0\n"); // R matrix rows
            writer.write("0.0,1.0,0.0\n");
            writer.write("0.0,0.0,1.0\n");
        }

        // Initialize a point cloud
        pointCloud = Arrays.asList(
            new Point(1.0f, 2.0f, 3.0f),
            new Point(4.0f, 5.0f, 6.0f),
            new Point(7.0f, 8.0f, 9.0f)
        );

        // Initialize a new ObjectSet with intrinsics and a point cloud
        os = new ObjectSet(intrinsics, pointCloud);
    }

    @Test
    void testObjectSetCreation() throws IOException, FileNotFoundException {
        assertNotNull(os, "ObjectSet should not be null after initialization");
        assertNotNull(os.objects, "ObjectSet should have a list of objects");
        assertTrue(os.objects.isEmpty(), "Initially, ObjectSet should be empty");
    }

    @Test
    void testReconcileCandidate() {
        // Create a candidate PointSet
        PointSet candidate = new PointSet(1, "object", 
            new Point(1.0f, 2.0f, 3.0f),
            new Point(4.0f, 5.0f, 6.0f)
        );

        os.reconcileCandidate(candidate, 0.5);

        assertEquals(1, os.objects.size(), "One object should be added or reconciled");
    }

    @Test
    void testToString() {
        assertEquals("ObjectSet of : 0 objects:", os.toString(), "Initial string representation should match expected format");

        // Add an object and check the string representation again
        os.reconcileCandidate(new PointSet(1, "vehicle", new Point(1.0f, 2.0f, 3.0f)), 0.5);
        assertTrue(os.toString().startsWith("ObjectSet of : 1 objects:"), "String representation should reflect the new object count");
    }
    @AfterEach
    void tearDown() {
        new File("test_intr.csv").delete(); // Clean up intrinsics file
        new File("test_pose.csv").delete(); // Ensure to delete pose file if it wasn't cleaned up earlier
    }
}
