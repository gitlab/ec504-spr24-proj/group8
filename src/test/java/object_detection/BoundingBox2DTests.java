package object_detection;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;
import object_detection.types.*;

class BoundingBox2DTests {

    @Test
    void testPointOnEachEdge() {
        BoundingBox2D box = new BoundingBox2D(0, 0, 10, 10, "vehicle");

        // Points on the boundaries of the box
        Point2D topEdge = new Point2D(5, 10);
        Point2D bottomEdge = new Point2D(5, 0);
        Point2D leftEdge = new Point2D(0, 5);
        Point2D rightEdge = new Point2D(10, 5);

        assertTrue(box.within(topEdge), "Point on top edge should be within");
        assertTrue(box.within(bottomEdge), "Point on bottom edge should be within");
        assertTrue(box.within(leftEdge), "Point on left edge should be within");
        assertTrue(box.within(rightEdge), "Point on right edge should be within");
    }

    @Test
    void testPointsJustOutsideEachEdge() {
        BoundingBox2D box = new BoundingBox2D(0, 0, 10, 10, "animal");

        // Points just outside the boundaries of the box
        Point2D leftOfBox = new Point2D(-1, 5);
        Point2D rightOfBox = new Point2D(11, 5);
        Point2D belowBox = new Point2D(5, -1);
        Point2D aboveBox = new Point2D(5, 11);

        assertFalse(box.within(leftOfBox), "Point just left of the bounding box should not be within");
        assertFalse(box.within(rightOfBox), "Point just right of the bounding box should not be within");
        assertFalse(box.within(belowBox), "Point just below the bounding box should not be within");
        assertFalse(box.within(aboveBox), "Point just above the bounding box should not be within");
    }

    @Test
    void testZeroWidthHeightBoundingBox() {
        BoundingBox2D box = new BoundingBox2D(5, 5, 0, 0, "object");

        // Points to test
        Point2D exactPoint = new Point2D(5, 5);
        Point2D nearbyPoint1 = new Point2D(5, 4);
        Point2D nearbyPoint2 = new Point2D(6, 5);

        assertTrue(box.within(exactPoint), "Exact point should be within");
        assertFalse(box.within(nearbyPoint1), "Nearby point should not be within");
        assertFalse(box.within(nearbyPoint2), "Nearby point should not be within");
    }

    @Test
    void testLargeCoordinates() {
        BoundingBox2D box = new BoundingBox2D(0, 0, 10000, 10000, "space");

        // Points to test
        Point2D wellWithin = new Point2D(5000, 5000);
        Point2D justOutside = new Point2D(10001, 5000);

        assertTrue(box.within(wellWithin), "Point well within large bounding box should be within");
        assertFalse(box.within(justOutside), "Point just outside large bounding box should not be within");
    }
}
