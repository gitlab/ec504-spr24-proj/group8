package object_detection;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import object_detection.types.*;

class PointSetTests {

    private PointSet ps;

    @BeforeEach
    void setUp() {
        ps = new PointSet(1, "vehicle", new Point(1.0f, 2.0f, 3.0f), new Point(4.0f, 5.0f, 6.0f));  // Initialize a PointSet with a sample ID, prediction name, and points
    }

    @Test
    void testPointSetCreation() {
        Point[] points = ps.getPoints();

        assertEquals(2, points.length, "PointSet should contain two points initially");

        assertEquals(1.0f, points[0].getX(), 0.001, "First point's X coordinate should match");
        assertEquals(2.0f, points[0].getY(), 0.001, "First point's Y coordinate should match");
        assertEquals(3.0f, points[0].getZ(), 0.001, "First point's Z coordinate should match");

        assertEquals(4.0f, points[1].getX(), 0.001, "Second point's X coordinate should match");
        assertEquals(5.0f, points[1].getY(), 0.001, "Second point's Y coordinate should match");
        assertEquals(6.0f, points[1].getZ(), 0.001, "Second point's Z coordinate should match");

        assertEquals("vehicle", ps.getPred(), "Prediction name should be 'vehicle'");
    }

    @Test
    void testAddPoint() {
        Point newPoint = new Point(7.0f, 8.0f, 9.0f);
        ps.addPoint(newPoint);

        Point[] points = ps.getPoints();

        assertEquals(3, points.length, "PointSet should contain three points after addition");
        assertEquals(7.0f, points[2].getX(), 0.001, "Third point's X coordinate should match");
        assertEquals(8.0f, points[2].getY(), 0.001, "Third point's Y coordinate should match");
        assertEquals(9.0f, points[2].getZ(), 0.001, "Third point's Z coordinate should match");
    }

    @Test
    void testGetIDX() {
        assertEquals(1, ps.getIDX(), "IDX should be 1 as specified during creation");
    }
}
