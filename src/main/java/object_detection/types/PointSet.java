package object_detection.types;

import object_detection.types.Point;

import java.util.*;

public class PointSet {

    Set<Point> pset;
    String predName;

    final int IDX;

    /**
     *
     * @param pp : the points that this PointSet will contain
     */
    public PointSet(int id, String predName, Point ...pp){
        this.pset = new HashSet<>();

        // add every point blankly to pointset
        this.pset.addAll(Arrays.asList(pp));
        this.IDX = id;
        this.predName = predName;
    }

    public void addPoint(Point p){
        pset.add(p);
    }

    public void addAll(Point ...p) {
        Collections.addAll(pset, p);
    }

    public void addAll(List<Point> p) {
        pset.addAll(p);
    }

    public String getPred(){
        return this.predName;
    }

    /*
     * This method is used to get the points in the PointSet
     */
    public Point[] getPoints(){
        Iterator<Point> iter = this.pset.iterator();
        Point[] res = new Point[this.pset.size()];

        for(int i = 0; i < this.pset.size(); i++){
            res[i] = iter.next();
        }

        return res;
    }

    public int getIDX() {
        return this.IDX;
    }
}
